package ru.java.rush.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.java.rush.entity.ProductEntity;
import ru.java.rush.service.ProductService;

import java.util.List;

@RestController
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/hello")
    public String getHelloWorld() {
        return "Hello World!";
    }

    @GetMapping("/products")
    public ResponseEntity<List<ProductEntity>> getAllProduct() {
        return ResponseEntity.ok().body(productService.findAll());
    }
}
