package ru.java.rush.util;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.java.rush.entity.ProductEntity;
import ru.java.rush.service.ProductService;

import java.util.ArrayList;
import java.util.List;

@Service
public class InitiateUtils implements CommandLineRunner {

    private final ProductService productService;

    public InitiateUtils(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void run(String... args) {

        List<ProductEntity> products = new ArrayList();
        products.add(new ProductEntity("Картофель", 20));
        products.add(new ProductEntity("Лукофель", 40));
        products.add(new ProductEntity("Яблофель", 95));

        productService.saveAll(products);
    }
}
